# Fluentd logger for PHP

## Requirements

- PHP 8.3 or higher
- fluentd v1.14 or higher

## Installation

### Using Composer

composer.json

```json
{
  "require": {
    "tiat/fluentd": "^1.0.0"
  }
}
```

# Usage

## PHP side

```php
<?php

require_once __DIR__.'/vendor/autoload.php';

use Tiat\Fluentd\FluentdLogger;
$log = new FluentdLogger('localhost');
$log->setTag('debug.test')->setData(['hello'=>'world']);
$log->init()->run();
```

Or

```php
<?php

require_once __DIR__.'/vendor/autoload.php';

use Tiat\Fluentd\FluentdLogger;
$log = new FluentdLogger('localhost');
$entity = ['tag'=>'debug.test', 'data'=>['hello'=>'world']];
$log->init(...$entity)->run();
```

## Fluentd side

Use `in_forward`.

```aconf
<source>
  @type forward
  port 24224
  bind 0.0.0.0
</source>

<match debug.test>
  @type stdout
</match>
```

You should always consider to use local fluentd proxy model.

````
apache2(mod_php)
fluent-logger-php
                 `-----proxy-fluentd
                                    `------aggregator fluentd
````

# Licence

BSD-3-Clause

# Contributors

Jan Theon
