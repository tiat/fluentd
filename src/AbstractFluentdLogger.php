<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Fluentd
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Fluentd;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractFluentdLogger implements FluentdLoggerInterface {
	
	/**
	 * @param    string    $host
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _getRequestHost(string $host) : string;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _connect() : bool;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _close() : bool;
	
	/**
	 * @param    string    $data
	 *
	 * @return bool|int
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _write(string $data) : bool|int;
	
	/**
	 * @param    FluentdEntityInterface    $entity
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _runPost(FluentdEntityInterface $entity) : bool;
}
