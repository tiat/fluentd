<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Fluentd
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Fluentd;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class FluentdEntity implements FluentdEntityInterface {
	
	/**
	 * @param    string      $_tag
	 * @param    array       $_data
	 * @param    null|int    $_time
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(private readonly string $_tag, private readonly array $_data, private ?int $_time = NULL) {
		if(empty($this->_time)):
			$this->_time = time();
		endif;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getTag() : ?string {
		return $this->_tag ?? NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getData() : ?array {
		return $this->_data ?? NULL;
	}
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getTime() : ?int {
		return $this->_time ?? NULL;
	}
}
