<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Fluentd
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Fluentd;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface FluentdEntityInterface {
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getTag() : ?string;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getData() : ?array;
	
	/**
	 * Return unixtime
	 *
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getTime() : ?int;
}
