<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Fluentd
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Fluentd;

//
use JsonException;
use Tiat\Fluentd\Exception\InvalidArgumentException;
use Tiat\Fluentd\Exception\RuntimeException;

use function fclose;
use function fwrite;
use function implode;
use function in_array;
use function is_resource;
use function json_encode;
use function register_shutdown_function;
use function sprintf;
use function str_contains;
use function stream_get_meta_data;
use function stream_set_timeout;
use function stream_socket_client;
use function strlen;
use function strpos;
use function strtolower;
use function substr;
use function trim;
use function usleep;

use const STREAM_CLIENT_CONNECT;
use const STREAM_CLIENT_PERSISTENT;

/**
 * FluentdLogger
 * <code>
 * require_once __DIR__.'/vendor/autoload.php';
 * use Tiat\Fluentd\FluentdLogger;
 * $log = new FluentdLogger('localhost');
 * $log->setTag('debug.test')->setData(['hello'=>'world']);
 * $log->init()->run();
 * </code>
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class FluentdLogger extends AbstractFluentdLogger {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int CONNECTION_TIMEOUT = 3;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int MAX_WRITE_RETRY = 5;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int SOCKET_TIMEOUT = 3;
	
	/**
	 * 1000 means 0.001 sec
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const int USLEEP_WAIT = 1000;
	
	/**
	 * Backoff strategies: default usleep
	 * attempts | wait
	 * 1        | 0.003 sec
	 * 2        | 0.009 sec
	 * 3        | 0.027 sec
	 * 4        | 0.081 sec
	 * 5        | 0.243 sec
	 *
	 * @since   3.0.0 First time introduced.
	 **/
	final public const int BACKOFF_TYPE_EXPONENTIAL = 0x01;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int BACKOFF_TYPE_USLEEP = 0x02;
	
	/**
	 * @var array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_options = ['socket_timeout' => self::SOCKET_TIMEOUT,
	                           'connection_timeout' => self::CONNECTION_TIMEOUT,
	                           'backoff_mode' => self::BACKOFF_TYPE_USLEEP, 'backoff_base' => 3,
	                           'usleep_wait' => self::USLEEP_WAIT, 'persistent' => FALSE, 'retry_socket' => TRUE,
	                           'max_write_retry' => self::MAX_WRITE_RETRY];
	
	/**
	 * @var array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_optionsAccept = ['socket_timeout', 'connection_timeout', 'backoff_mode', 'backoff_base',
	                                 'usleep_wait', 'persistent', 'retry_socket', 'max_write_retry'];
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_request;
	
	/**
	 * @var string[]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_protocolSupported = ['tcp', 'unix'];
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_entityTag;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_entityData;
	
	/**
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	private mixed $_socket;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_connectionError;
	
	/**
	 * @var FluentdEntityInterface
	 * @since   3.0.0 First time introduced.
	 */
	private FluentdEntityInterface $_entity;
	
	/**
	 * @param    string    $_host
	 * @param    int       $_port
	 * @param    array     $options
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(private readonly string $_host = self::DEFAULT_ADDRESS, private readonly int $_port = self::DEFAULT_PORT, array $options = []) {
		//
		register_shutdown_function([$this, 'shutdown']);
		
		//
		$this->_request = $this->setConnectionRequest($this->_host, $this->_port);
		
		//
		if(! empty($options)):
			$this->setOptions($options);
		endif;
	}
	
	/**
	 * @param    string    $host
	 * @param    int       $port
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionRequest(string $host, int $port) : ?string {
		if(( $pos = strpos($host, "://") ) !== FALSE):
			//
			$proto = strtolower(substr($host, 0, $pos));
			$host  = substr($host, $pos + 3);
			
			//
			if(! in_array($proto, $this->_protocolSupported, TRUE)):
				$msg = sprintf("Given protocol (%s) is not supported. Please use one of following: %s", $proto,
				               implode(', ', $this->_protocolSupported));
				throw new InvalidArgumentException($msg);
			endif;
			
			//
			if($proto === 'unix'):
				return "unix://" . $host;
			else:
				return sprintf("%s://%s:%d", $proto, $this->_getRequestHost($host), $port);
			endif;
		else:
			return sprintf("tcp://%s:%d", $this->_getRequestHost($host), $port);
		endif;
	}
	
	/**
	 * @param    string    $host
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getRequestHost(string $host) : string {
		// IPv6 address should be surrounded with brackets
		if(str_contains($host, "::")):
			return sprintf("[%s]", trim($host, "[]"));
		endif;
		
		//
		return $host;
	}
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __destruct() {
		$this->shutdown();
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
		if(! empty($this->_socket) && is_resource($this->_socket) && ! $this->getOption("persistent", FALSE)):
			$this->_close();
		endif;
	}
	
	/**
	 * @param    string        $key
	 * @param    mixed|NULL    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getOption(string $key, mixed $default = NULL) : mixed {
		return $this->_options[$key] ?? $default;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _close() : bool {
		if(is_resource($this->_socket)):
			return fclose($this->_socket);
		endif;
		
		// No resource, everything ok => no open connections
		return TRUE;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionInfo() : ?array {
		if(is_resource($this->_socket)):
			return stream_get_meta_data($this->_socket);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionError() : ?array {
		return $this->_connectionError ?? NULL;
	}
	
	/**
	 * @param    string    $key
	 * @param    mixed     $value
	 * @param    bool      $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setOption(string $key, mixed $value, bool $override = FALSE) : static {
		if(! in_array(( $key = strtolower($key) ), $this->_optionsAccept, TRUE)):
			$msg = sprintf("Key '%s' is not supported option.", $key);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		if(! isset($this->_options[$key]) || $override === TRUE):
			$this->_options[$key] = $value;
		else:
			$msg = sprintf("Key '%s' already exists and override is not allowed.", $key);
			throw new RuntimeException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	public function getOptions() : ?array {
		return $this->_options ?? NULL;
	}
	
	/**
	 * @param    array    $options
	 * @param    bool     $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setOptions(array $options, bool $override = FALSE) : static {
		foreach($options as $key => $val):
			$this->setOption($key, $val, $override);
		endforeach;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $data
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setData(array $data) : static {
		//
		$this->_entityData = $data;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $tag
	 *
	 * @return FluentdLoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setTag(string $tag) : FluentdLoggerInterface {
		//
		$this->_entityTag = $tag;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : static {
		//
		if(( $entity = $args['entity'] ?? NULL ) !== NULL):
			//
			if(! $entity instanceof FluentdEntityInterface):
				throw new InvalidArgumentException("Entity must be instance of FluentdEntityInterface.");
			endif;
			
			//
			$this->_entity = $entity;
		elseif(( $tag = $args['tag'] ?? $this->getTag() ?? NULL ) !== NULL &&
		       ( $data = $args['data'] ?? $this->getData() ?? NULL ) !== NULL):
			//
			$this->_entity = new FluentdEntity($tag, $data);
		else:
			throw new RuntimeException('You must define either entity or tag & data values.');
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getTag() : ?string {
		return $this->_entityTag ?? NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getData() : ?array {
		return $this->_entityData ?? NULL;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function run() : bool {
		if(! empty($this->_entity)):
			return $this->_runPost($this->_entity);
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * Make actual POST
	 *
	 * @param    FluentdEntityInterface    $entity
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _runPost(FluentdEntityInterface $entity) : bool {
		//
		if(! empty($packed = $this->pack($entity))):
			$buffer  = $packed;
			$length  = strlen($packed);
			$written = 0;
			$retry   = 0;
			
			//
			if($this->_connect() === TRUE):
				//
				while($written < $length):
					//
					$result = $this->_write($buffer);
					
					//
					if($result === 0):
						//
						if(! $this->getOption("retry_socket", TRUE)):
							$msg = sprintf("Count not send message with %s tag.", $this->getTag());
							throw new RuntimeException($msg);
						endif;
						
						//
						if($retry > $this->getOption("max_write_retry", self::MAX_WRITE_RETRY)):
							$msg = sprintf("Failed: retry count exceeds limit %u", $retry);
							throw new RuntimeException($msg);
						endif;
						
						//
						if($this->getOption('backoff_mode', self::BACKOFF_TYPE_EXPONENTIAL) ===
						   self::BACKOFF_TYPE_EXPONENTIAL):
							$this->backoffExponential(3, $retry);
						else:
							usleep($this->getOption("usleep_wait", self::USLEEP_WAIT));
						endif;
						
						//
						$retry++;
						
						//
						continue;
					elseif($result === FALSE):
						throw new RuntimeException("Could not write message");
					elseif(empty($result)):
						throw new RuntimeException("Connection aborted");
					endif;
					
					//
					$written += $result;
					$buffer  = substr($packed, $written);
				endwhile;
			endif;
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @param    FluentdEntityInterface    $entity
	 *
	 * @return false|string
	 * @since   3.0.0 First time introduced.
	 */
	public function pack(FluentdEntityInterface $entity) : false|string {
		try {
			return json_encode([$entity->getTag(), $entity->getData(), $entity->getTime()], JSON_THROW_ON_ERROR);
		} catch(JsonException $e) {
			throw new RuntimeException($e->getMessage());
		}
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _connect() : bool {
		//
		$connectOptions = STREAM_CLIENT_CONNECT;
		
		//
		if($this->getOption("persistent", FALSE)):
			$connectOptions |= STREAM_CLIENT_PERSISTENT;
		endif;
		
		//
		if(( $socket = @stream_socket_client($this->_request, $errno, $errstr,
		                                     $this->getOption("connection_timeout", self::CONNECTION_TIMEOUT),
		                                     $connectOptions) ) !== FALSE):
			//
			stream_set_timeout($socket, $this->getOption("socket_timeout", self::SOCKET_TIMEOUT));
			
			//
			$this->_socket = $socket;
		else:
			// Save to connection error array
			$this->_connectionError = [$errno => $errstr];
			
			// Throw exception
			$msg = sprintf("Connection has been failed: %s (%u).", $errstr, $errno);
			throw new RuntimeException($msg);
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @param    string    $data
	 *
	 * @return bool|int
	 * @since   3.0.0 First time introduced.
	 */
	protected function _write(string $data) : bool|int {
		return @fwrite($this->_socket, $data);
	}
	
	/**
	 * @param    int    $base
	 * @param    int    $attempt
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function backoffExponential(int $base, int $attempt) : void {
		usleep(( $base ** $attempt ) * 1000);
	}
}
