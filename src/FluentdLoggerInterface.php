<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Fluentd
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Fluentd;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface FluentdLoggerInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int    DEFAULT_PORT = 24224;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int    DEFAULT_PORT_HTTP = 9880;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_ADDRESS = '127.0.0.1';
	
	/**
	 * @param    array    $options
	 * @param    bool     $override
	 *
	 * @return FluentdLoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOptions(array $options, bool $override = FALSE) : FluentdLoggerInterface;
	
	/**
	 * Set singleton option
	 *
	 * @param    string    $key
	 * @param    mixed     $value
	 * @param    bool      $override    Override existing value
	 *
	 * @return FluentdLoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOption(string $key, mixed $value, bool $override = FALSE) : FluentdLoggerInterface;
	
	/**
	 * Get all options
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getOptions() : ?array;
	
	/**
	 * Get singleton option
	 *
	 * @param    string        $key
	 * @param    mixed|NULL    $default
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getOption(string $key, mixed $default = NULL) : mixed;
	
	/**
	 * @param    string    $host
	 * @param    int       $port
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionRequest(string $host, int $port) : ?string;
	
	/**
	 * @param    array    $data
	 *
	 * @return FluentdLoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setData(array $data) : FluentdLoggerInterface;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getData() : ?array;
	
	/**
	 * @param    string    $tag
	 *
	 * @return FluentdLoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setTag(string $tag) : FluentdLoggerInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getTag() : ?string;
	
	/**
	 * @param    mixed    ...$args
	 *
	 * @return FluentdLoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : FluentdLoggerInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function run() : bool;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionInfo() : ?array;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionError() : ?array;
	
	public function backoffExponential(int $base, int $attempt) : void;
	
	public function shutdown() : void;
	
	public function pack(FluentdEntityInterface $entity) : false|string;
}
